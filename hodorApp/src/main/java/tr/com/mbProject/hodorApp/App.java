package tr.com.mbProject.hodorApp;

import java.util.logging.Level;
import java.util.logging.Logger;

import io.swagger.client.ApiClient;
import io.swagger.client.api.VehiclesApi;
import io.swagger.client.model.Vehicle;
import io.swagger.client.model.VehicleDetail;
import io.swagger.client.model.Vehicles;
import tr.com.mbProject.hodorApp.processor.DoorHoldProcessor;
import tr.com.mbProject.hodorApp.utils.TokenManager;
/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) {
		try {
			


			ApiClient.setAuthToken(TokenManager.getInstance().getAccessToken());
			

			VehiclesApi api = new VehiclesApi();
			Vehicles vehicles = api.getAllVehicles();
			
			for (Vehicle vehicle : vehicles) {
				VehicleDetail detail = api.getVehicleById(vehicle.getId());
				
				System.out.println("Car Observer Process will be started For; ");
				System.out.println(detail);
				
				DoorHoldProcessor dhp = new DoorHoldProcessor(vehicle.getId());
				dhp.run();
			}

		} catch (Exception ex) {
			Logger.getLogger(App.class.getName()).log(Level.WARNING, null, ex);
		}
	}
}
