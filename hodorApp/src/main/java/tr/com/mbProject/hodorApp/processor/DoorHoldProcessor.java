package tr.com.mbProject.hodorApp.processor;

import java.util.Date;

import io.swagger.client.ApiClient;
import io.swagger.client.ApiException;
import io.swagger.client.api.DoorsApi;
import io.swagger.client.api.TiresApi;
import io.swagger.client.model.DoorLockChangeRequestBody;
import io.swagger.client.model.DoorLockChangeRequestBody.CommandEnum;
import io.swagger.client.model.Tires;
import tr.com.mbProject.hodorApp.exceptions.CrashDetectedException;
import tr.com.mbProject.hodorApp.utils.TokenManager;

public class DoorHoldProcessor extends Thread {

	private String carId;
	
	private double sumOfPrevious;
	private double sumOfNow;
	
	private boolean toBeContinue = true;
	
	
	private boolean firstRun = true;

	public DoorHoldProcessor(String carId) {
		this.carId = carId;

	}
	
	public void stopMe() {
		toBeContinue=false;
	}
	
	public void run() {

		try {

			TiresApi tapi = new TiresApi();

			while (toBeContinue) {
				ApiClient.setAuthToken(TokenManager.getInstance().getAccessToken());
				Tires tire = tapi.getTiresStatus(carId);
				tire.getTirepressurefrontleft().getValue();

				System.out.println("Front Left/Right / Rear Left/Right >>> " 
						+ tire.getTirepressurefrontleft().getValue() + " / " 
						+ tire.getTirepressurefrontright().getValue() + " / "
						+ tire.getTirepressurerearleft().getValue() + " / "
						+ tire.getTirepressurerearright().getValue() + " >>> " + new Date());
				
				
				
				if(firstRun) {
					firstRun = false;
					sumOfNow =	tire.getTirepressurefrontleft().getValue()  
							  + tire.getTirepressurefrontright().getValue() 
							  + tire.getTirepressurerearleft().getValue()  
							  + tire.getTirepressurerearright().getValue();
				}
				
				
				sumOfPrevious = sumOfNow;
				sumOfNow =	tire.getTirepressurefrontleft().getValue()  
						  + tire.getTirepressurefrontright().getValue() 
						  + tire.getTirepressurerearleft().getValue()  
						  + tire.getTirepressurerearright().getValue();
			
				
				if(sumOfPrevious > sumOfNow &&	((sumOfPrevious - sumOfNow)/sumOfPrevious)*100 > 20) {
					stopMe();
					throw new CrashDetectedException("WARNING CRASH DETECTED.");
				}
				
				Thread.sleep(15*1000);
			}

		} catch (ApiException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (CrashDetectedException e) {
			try {
				System.out.println("Detected crash on Car :" + carId + " Doors will be unlocked.");
				
				DoorsApi dapi = new DoorsApi();
				DoorLockChangeRequestBody dlcrb = new DoorLockChangeRequestBody();
				dlcrb.setCommand(CommandEnum.UNLOCK);
				
				dapi.postDoors(carId, dlcrb);
				System.out.println("Doors unlocked.");
			} catch (ApiException e1) {
				e1.printStackTrace();
			}
		}
	}
}