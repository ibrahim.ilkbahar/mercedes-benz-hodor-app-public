package tr.com.mbProject.hodorApp.exceptions;

public class CrashDetectedException extends Exception {
	public CrashDetectedException(String message) {
		super(message);
	}
}
