package tr.com.mbProject.hodorApp.utils;

import pet.jen.mbdev.api.TokenProvider;
import pet.jen.mbdev.api.auth.AuthorizationFlowHandler;

public class TokenManager {
	private static TokenProvider instance = null;

	private TokenManager() {}

	static {
		try {
			if (instance == null) {
				SampleConfig config = ConfigParser.getConfig();
				AuthorizationFlowHandler handler = AuthorizationFlowHandler.setup(config.getOAuthConfig());
				instance = handler.authorize(config.getUsername(), config.getPassword());
			}
		} catch (Exception e) {
			throw new RuntimeException("Exception occured while retriving token");
		}
	}

	public static TokenProvider getInstance() {
		return instance;
	}
}
